package com.ksmt.alarmnotify;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class AlarmAlert extends Activity {

	private String notify_msg = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Waking up lock screen 
		getWindow().addFlags(/*WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | */
							 WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED   |
							 WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		
		// Unlock phone (depressed)
		//KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
		//KeyguardLock kl = km.newKeyguardLock("MyKeyguardLock");
		//kl.disableKeyguard();

		// Get the message from service
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			notify_msg = extras.getString("nofity_msg");
			Log.e(Dbg._TAG_(), notify_msg);
		}
		showAlarm(notify_msg);
		
		setContentView(R.layout.alarm_alert);
	}

	private void showAlarm(String msg) {
		new AlertDialog.Builder(AlarmAlert.this)
        .setTitle(getString(R.string.alarm_dia_title))
        .setMessage(msg)
        .setCancelable(false)      
        .setPositiveButton(getString(R.string.alarm_dia_open), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
        		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        		startActivity(intent);
        		finish();
            }
        })
        /*
        .setNeutralButton(getString(R.string.alarm_dia_name), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
        		finish();
            }
        })
        */          
        .setNegativeButton(getString(R.string.alarm_dia_close), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish(); // dialog.cancel();
            }
        })
        .show();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		// Necessary:
		// Set the string passed from the service to the original intent, or
		// you won't get the message passed from service
		setIntent(intent);
	}
}
