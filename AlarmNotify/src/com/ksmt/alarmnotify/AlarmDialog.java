package com.ksmt.alarmnotify;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class AlarmDialog extends Dialog implements android.view.View.OnClickListener  {
	
	private Activity dialog;
	private Button btn_yes, btn_no;

	public AlarmDialog(Activity context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.dialog = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alarm_dialog);
		btn_yes = (Button) findViewById(R.id.btn_yes);
		btn_no = (Button) findViewById(R.id.btn_no);
		btn_yes.setOnClickListener(this);
		btn_no.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.btn_yes:
			dialog.finish();
			break;
		case R.id.btn_no:
			break;
		default:
			break;
		}
		dismiss();
	}
}
