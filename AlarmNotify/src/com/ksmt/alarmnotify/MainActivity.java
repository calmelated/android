package com.ksmt.alarmnotify;

import com.ksmt.alarmnotify.MainActivity;
import com.ksmt.alarmnotify.R;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		//Log.e(toString(), "On Stop state");
	} 

	@Override
	protected void onResume(){
		super.onResume();
		//Log.e(this.toString(), "On broadcast receiver registered!");
	}

	@Override
	protected void onPause(){
		super.onPause();
		//Log.e(this.toString(), "On Pause state");
	} 
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
        //Log.e(this.toString(), "broadcast UNregistred!");
	}		

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void newAlarmBtnClicked(View v) {
		AlarmReceiver alarm = new AlarmReceiver();
		alarm.setAlarm(this);
		finish();
	}

	public void customToastBtnClicked(View v) {
		AlarmToast.makeToast(this, "Custom Toast Message");
	}

	public void loopToastBtnClicked(View v) {
		AlarmToast.loopToast(this, 10000, "Loop Toast Message");
	}

	public void alarmDiaBtnClicked(View v) {
		new AlarmDialog(this).show();
	}
	
}
