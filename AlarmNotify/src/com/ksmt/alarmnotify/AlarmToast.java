package com.ksmt.alarmnotify;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public abstract class AlarmToast {
	public static void makeToast(Context context, CharSequence msg) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) context.findViewById(R.id.toast_layout));
        View layout = inflater.inflate(R.layout.custom_toast, null);
        TextView tView = (TextView) layout.findViewById(R.id.toast_text_1);
        tView.setText(msg);

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
	}

	public static void loopToast(final Activity context, int duraction, final CharSequence msg) {
		if (duraction == 0) {
			AlarmToast.makeToast(context, msg);
			return;
		}

		// infinite toast
		new CountDownTimer(duraction, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				AlarmToast.makeToast(context, msg);
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				AlarmToast.makeToast(context, msg);
			}
		}.start();
	}	

}
